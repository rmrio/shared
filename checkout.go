// This module contains general shared structure
// The checkout is an abstraction for represent user checkout process on a shop
package shared

import "time"

// Represents transaction state on the gate and billy, the is same
type GateOrderStatus int

const (
	Approved   GateOrderStatus = iota // Transaction is approved, final status
	Declined                          // Transaction is declined, final status
	Error                             // Transaction is declined but something went wrong, final status
	Filtered                          // Transaction is declined by fraud control systems, final status
	Processing                        // Transaction is being processed, should continue polling, non final status
	Unknown                           // The status of transaction is unknown, non final status
)

var orderStatuses = [...]string{
	"approved",
	"declined",
	"error",
	"filtered",
	"processing",
	"unknown",
}

func (s GateOrderStatus) String() string { return orderStatuses[s] }

// Represents 3D Secure status
type Status3D int

const (
	Auth3D Status3D = iota
	NotAuth3D
	Unsupported3D
	Unknown3D
)

var statuses3D = [...]string{
	"AUTHENTICATED",
	"NOT_AUTHENTICATED",
	"UNSUPPORTED",
	"UNKNOWN",
}

func (s Status3D) String() string { return statuses3D[s] }

// Shop side checkout
type Checkout struct {
	CreatedAt    time.Time `json:"createdAt"`
	Domain       string    `json:"domain"`
	AffId        int       `json:"affId"`
	Tracking     string    `json:"tracking"` // additional affiliate tracking/marker
	Amount       float64   `json:"amount"`
	Currency     string    `json:"currency"` // 3 chars currency code
	ShippingCost float64   `json:"shippingCost"`
	ShippingCode string    `json:"shippingCode"`
	Email        string    `json:"email"`

	Cart          string  `json:"cart"`
	Coupon        string  `json:"coupon"` // discount coupon
	CouponAmount  float64 `json:"couponAmount"`
	CouponPercent float64 `json:"couponPercent"`

	ShippingAddress string `json:"shipping"`

	BillingAddress string `json:"billing"`

	Card      CreditCard `json:"card"`
	IP        string     `json:"ip"`
	UserAgent string     `json:"userAgent"`
	Uuid      string     `json:"uuid"` // Uuid value from session cart
}

type Address struct {
	FirstName     string `json:"firstName"`
	LastName      string `json:"lastName"`
	StreetAddress string `json:"streetAddress"`
	City          string `json:"city"`
	Zip           string `json:"zip"`
	Country       string `json:"country"`
	State         string `json:"state"`
	Phone         string `json:"phone"`
}

// Extended cart presentation. Its give possible to create order descriptions
// (as example for emails) without asking the hope service.
// NOTE: the sku in Items, Descs and Masks should be int, because is represent
// the pack id, but it is not json serializable, so string type used.
// Maybe use string everywhere, in hopeclient too ?
type ShoppingCart struct {
	Items  map[string]int     `json:"items" sql:"type:json"`  // sku => count
	Descs  map[string]string  `json:"descs" sql:"type:json"`  // sku => desc
	Prices map[string]float64 `json:"prices" sql:"type:json"` // sku => price
}

type CreditCard struct {
	ID          uint      `json:"-" gorm:"primary_key"`
	CreatedAt   time.Time `json:"-"`
	PrintedName string    `json:"printedName"`
	Number      string    `json:"number"`
	ExpireMonth int       `json:"expireMonth"`
	ExpireYear  int       `json:"expireYear"`
	CVV2        string    `json:"cvv2" gorm:"column:cvv2"` // may be starts from 0
}
